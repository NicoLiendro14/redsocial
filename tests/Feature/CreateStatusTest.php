<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class CreateStatusTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    /*public function guestsUserCanNotCreateStatuses()
    {
        //FIX ME: Revisar problema con el auth.
        $this->withoutExceptionHandling();
        $response = $this->post(route('statuses.store'), ['body' => 'Mi primer status']);
        $response->assertRedirect('login');
    }*/
    /** @test */
    public function aUserCanCreateStatuses()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $response = $this->post(route('statuses.store'), ['body' => 'Mi primer status']);
        $response->assertJson([
            'data' => ['body' => 'Mi primer status']
        ]);
        $this->assertDatabaseHas('statuses', [
            'user_id' => $user->id,
            'body' => 'Mi primer status'
        ]);
    }

    /** @test */
    public function aStatusRequireABody()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $response = $this->postJson(route('statuses.store'), ['body' => '']);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message', 'errors' => ['body']
        ]);
    }

    /** @test */
    public function aStatusRequireAMinimumLength()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $response = $this->postJson(route('statuses.store'), ['body' => 'abcd']);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message', 'errors' => ['body']
        ]);
    }
}
