<?php

namespace Tests\Browser;

use App\User;
use Carbon\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class UsersCanCreateStatusesTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * 
     * @test
     */
    public function usersCanCreateStatuses()
    {
        $user = factory(User::class)->create();
        $this->browse(function (Browser $browser) use ($user) {

            $browser->loginAs($user)
                ->visit('/')
                ->type('body', 'Mi primer status')
                ->press('#create-status')
                ->screenshot("texto")
                ->waitForText('Mi primer status')
                ->screenshot("despues")
                ->assertSee('Mi primer status')
                ->assertSee($user->name);
        });
    }
}
