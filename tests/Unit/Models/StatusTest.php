<?php

namespace Tests\Unit\Models;

use App\Models\Like;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Status;
use App\User;

class StatusTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function aStatusBelongsToAUser()
    {
        $status = factory(Status::class)->create();

        $this->assertInstanceOf(User::class, $status->user);
    }

    /** @test */

    public function aStatusHasManyLikes()
    {
        $status = factory(Status::class)->create();

        factory(Like::class)->create(['status_id' => $status->id]);
        $this->assertInstanceOf(Like::class, $status->likes->first());
    }
}
