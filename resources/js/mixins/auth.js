let user = document.head.querySelector('meta[name="user"]');

module.exports = {
    computed: {
        currentUser() {
            return JSON.parse(user.content);
        },
        isAuthenticated() {
            //Retorna false si no tiene contenido
            return !!user.content;
        },
        guest() {
            return !this.isAuthenticated;
        }
    },
};
